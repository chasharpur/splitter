/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

var fs = require('fs');
var path = require('path');
var utils = require('./utils');
class menu {
    static clean_name( name ) {
        if ( name.indexOf("@") != -1 ) 
            return name.substring(0,name.indexOf("@"));
        else
            return name;
    }
    /**
     * Is this a hidden menu item?
     * @param dir the directory to search
     * @return true if there is a hidden file in the dir
     */
    static get_hidden( dir ) {
        let hidden_file = path.join(dir,".hidden");
        return fs.existsSync(hidden_file);
    }
    static is_menu_item( dir ) {
        let contents = fs.readdirSync(dir);
        for ( var i=0;i<contents.length;i++ ) {
            let full_path = path.join(dir,contents[i]);
            let stats = fs.statSync(full_path);
            if ( stats.isFile() && utils.is_number(contents[i]) )
                return true;
        }
        return false;
    }
     /**
     * Get the parameters this menu item wants
     * @param dir the directory to search
     * @return a space-delimited string of wanted url parameters or empty string
     */
    static get_wants( dir ) {
        let wants_file = path.join(dir,".wants");
        if ( fs.existsSync(wants_file) ) {
            let data = fs.readFileSync(wants_file,{encoding:"utf8"});
            return data.toString().trim();
        }
        else
            return "";
    }
    /**
     * Is this a https menu item?
     * @param dir the directory to search
     * @return true if there is a https file in the dir
     */
    static get_https( dir ) {
        let https_file = path.join(dir,".https");
        return fs.existsSync(https_file);
    }
    static get_item_index( dir ) {
        let index = 0;
        let files = fs.readdirSync(dir);
        for ( let i=0;i<files.length;i++ ) {
            let name = path.basename(files[i]);
            if ( utils.is_number(name) ) {
                index = parseInt(name);
                break;
            }
        }
        return index;
    }
    /**
     * Build a list of menu items in a directory
     * @param dir the directory to scan
     * @return the complete list as a JSON array
     */
    static get_item_list( dir ) {
        let list = Array();
        let files = fs.readdirSync(dir)
        for ( let i=0;i<files.length;i++ ) {
            let name = files[i];
            let full_path = path.join(dir,name);
            let stats = fs.statSync(full_path);
            if ( stats.isDirectory() && menu.is_menu_item(full_path))  {
                let index = menu.get_item_index(full_path);
                let hidden = menu.get_hidden(full_path);
                let https = menu.get_https(full_path);
                let wants_params = menu.get_wants(full_path);
                let item = {};
                item.name = menu.clean_name(name);
                item.path = name;
                item.index = index;
                if ( hidden )
                    item.hidden = hidden;
                if ( https )
                    item.https = https;
                if ( wants_params.length>0 )
                    item.wants = wants_params;
                let sub_list = menu.get_item_list(full_path);
                if ( sub_list.length > 0 )
                    item.items = sub_list;
                list.push(item);
            }
        }
        list.sort(function(a,b){
            if ( a.index > b.index )
                return 1;
            else if ( a.index < b.index )
                return -1;
            else
                return 0;
        });
        return list;
    }
    /**
     * build the site menu from scratch
     * @param web_root the root of the web directory path
     * @param siteprefix the key into the cache, the site-prefix
     * @return a JSON object representing the menu
     */
    static build_menu(web_root,siteprefix){
        var my_menu = {};
        var site_root = path.join(web_root,siteprefix);
        if ( !fs.existsSync(site_root) )
            throw "unknown site prefix "+siteprefix;
        else {
            let list = menu.get_item_list(site_root);
            my_menu.items = list;
            my_menu.time = new Date().getTime();
            menu.cache[siteprefix] = my_menu;
        }
        return my_menu;
    }
    /**
     * get the complete menus for a site, probably from the cache
     * @param root the root of the web directory path
     * @param q the parsed get params
     * @return a JSON object representing the menu
     */
    static get(root,q) {
        let my_menu;
        if ( q.siteprefix in menu.cache ) {
            my_menu = menu.cache[q.siteprefix];
            if ( my_menu.hasOwnProperty("time") ) {
                let then = menu.time;
                let now = new Date().getTime();
                if ( now-then > 1000000 ) {
                    delete menu.cache[q.siteprefix];
                    return menu.build_menu(q.web_root,q.siteprefix);
                }
            }
            else
                my_menu.time = new Date().getTime();
            return my_menu;
        }
        else
            return menu.build_menu(q.web_root,q.siteprefix);
    }
}
// this should persist across calls
menu.cache = {};
module.exports = menu;
