/**
 * Outer handler for splitting of XML files into layers
 */
class splitter {
    constructor( target, projid ){
        this.target = target;
        this.projid = projid;
        this.build_html();
        this.install_handlers();
    }
    /**
     * Compute the maximum layer number of an array of layers
     * @return an int
     */
    max_layer( arr ){
        let maxi = 0;
        for ( let i=0;i<arr.length;i++ )
            if ( arr[i].layer > maxi )
                maxi = arr[i].layer;
        return Math.round(maxi);
    }
    /**
     * Construct a select menuof simple integers
     * @param mini minimum int value
     * @param maxi maximum int value
     * @param value to act as default
     * @return the selct menu as HTML text
     */
    range_drop_down( mini, maxi, def_value ){
        let select = '<select class="select_layer">';
        for ( let i=mini;i<=maxi;i++ ) {
            if ( i == def_value )
                select += '<option value="'+i+'" selected="selected">'+i+'</option>';
            else
                select += '<option value="'+i+'">'+i+'</option>';
        }
        select += '</select>';
        return select;
    }
    /**
     * Set the width of a results header cell
     * @param id the id of the results cell
     */
    set_header_cell_width(id) {
        let th = $("#results-header th:eq("+id+")");
        let td = $("#results tr:eq(0) td:eq("+id+")");
        let wd = td.width();
        th.width(wd);
        let td_l = td.position().left;
        let left_pos = td_l;
        th.css("left",left_pos+"px");
    }
    /**
     * Simplify a name to a string without spaces all lowercase
     * @param str a user-entered string like a name
     */
    simplify(str) {
        str = str.toLowerCase();
        str.replace(/ /g,"_");
    }
    /**
     * Format the original XML with line-numbers
     * @param original the original XML before splitting
     * @return HTML
     */
    format_original_xml(original) {
        let html = '<pre id="original">';
        let res3 = original.replace(/</g,"&lt;");
        let res4 = res3.replace(/>/g,"&gt;");
        let lines = res4.split("\n");
        for ( let i=0;i<lines.length;i++ ) {
            html += '<span>'+lines[i]+'</span>\n';
        }
        html += '</pre>';
        return html;
    }
    /**
     * format the results as XML
     * @param results an array of split XML files
     * @param original the original xml file contents
     */
    format_xml(results,original){
        let html = '<div id="tab-container">';
        for ( let i=0;i<results.length;i++ )
        {
            let lay = (i==results.length-1)?"layer-final":"layer-"+(i+1);
            html += '<pre id="'+lay+'" class="tab-content';
            if ( i==0 )
                html += ' initial';
            html += '">';
            let res1 = results[i].replace(/</g,"&lt;");
            let res2 = res1.replace(/>/g,"&gt;");
            let lines = res2.split("\n");
            for ( let j=0;j<lines.length;j++ )
                html += '<span>'+lines[j]+'</span>\n';
            html += '</pre>';
        }
        html += '</div>';
        html += this.format_original_xml(original);
        return html;
    }
    /**
     * Compute the horizontal padding of an element
     * @param obj the $ object
     * @return an integer
     */
    h_padding( obj ){
        let l_pad = parseInt(obj.css("padding-left"));
        let r_pad = parseInt(obj.css("padding-right"));
        return r_pad+l_pad;
    }
    /**
     * The split results are a set of split XML files
     * @param results an array of XML split files
     * @param original the original XML file
     */
    format_split_results(results,original){
        $("#results-container").remove();
        $("#tabs-container").remove();
        $("#save_scan").remove();
        $("#"+this.target).append('<div id="tabs-container"></div>');
        // create the tabs menu
        let html = '<ul class="tabs-menu">'
        for ( let i=0;i<results.length;i++ ) {
            let lay = (i==results.length-1)?"layer-final":"layer-"+(i+1);
            html += '<li';
            if ( i==0 )
                html +=' class="current"';
            html += '><a href="#'+lay+'">'+lay+'</a></li>';
        }
        html +='</ul>';
        // now format the results
        html += this.format_xml(results,original);
        $("#tabs-container").append(html);
        let o_pad = this.h_padding($("#original"));
        let l_pad = this.h_padding($(".tab-content").first());
        $("#original").width($("#original").width()-o_pad);
        $(".tab-content").width($(".tab-content").width()-l_pad);
        let menu_ht = $(".tabs-menu").height();
        let tabs_ht = $(".tab-content").first().outerHeight();
        let orig_ht = $("#original").outerHeight();
        // shrink to fit
        let tabs_pos = Math.round($("#tabs-container").offset().top);
        let orig_pos = Math.round($("#original").offset().top);
        let gap = orig_pos - (tabs_pos+menu_ht+tabs_ht);
        let remain_ht = $(window).height()-(tabs_pos+menu_ht+gap);
        let t_pad_ht = $(".tab-content").outerHeight()-$(".tab-content").height();
        let o_pad_ht = $("#original").outerHeight()-$("#original").height();
        tabs_ht = Math.round(remain_ht/2-t_pad_ht);
        $(".tab-content").height(tabs_ht);
        orig_ht = Math.round(remain_ht/2-o_pad_ht);
        $("#original").height(orig_ht);
        // activate tabs
        $(".tabs-menu a").click(function(event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            let tab = $(this).attr("href");
            $(".tab-content").not(tab).css("display", "none");
            $(tab).fadeIn();
        });
    }
    /**
     * Get the height of NRows of the results table
     * @param n_rows the number of rows to measure
     * @return the height of the first n_rows of the table
     */
    measure_n_rows(n_rows){
        let limit = 0;
        let ht_n_rows = 0;
        $("#results tbody tr").each(function(){
            ht_n_rows += $(this).height();
            if ( ++limit == n_rows )
                return false;
        });
        return ht_n_rows;
    }
    /**
     * Compute the total height of the results header
     * @return the height 
     */
    results_header_ht(){
        return $("#results-header th").outerHeight();
    }
    curtail_left(context,room){
        if ( room >= 0 ) {
            while ( context.length > room ){
                let index = context.indexOf(" ");
                if ( index != -1 )
                    context = context.substring(index+1);
            }
        }
        return context;
    }
    /**
     * Remove all XML tags
     */
    strip_xml(xml){
        return xml.replace(/<[^>]+>/g,'');
    }
    layers_ok(){
        let ok = true;
        $(".select_layer").each(function(){
            if ( parseInt($(this).val())==0 )
                ok = false;
        });
        return ok;
    }
    format_scan_results(results,original){
        var self = this;
        $("#results-container").remove();
        $("#tabs-container").remove();
        $("#save_split").remove();
        $("#"+this.target).append('<div id="results-container"></div>');
        if ( results.length > 0 ) {
            $("#results-container").append('<table id="results-header"></table>');
            $("#results-header").append('<thead><tr><th>Pattern</th><th>Context</th>'
            +'<th>Line</th><th>Layer</th><th>&nbsp;</th></tr></thead>');
            $("#results-container").append(
                '<div id="results-wrapper"><table id="results"></table></div>');
            let table =$("#results");
            let rows = "";
            for ( let i=0;i<results.length;i++ ) {
                let left_context = results[i].left_context;
                let core = this.strip_xml(results[i].core);
                if ( left_context.length+core.length > 70 )
                    left_context = this.curtail_left(left_context,70-core.length);
                let row = '<tr><td>'+results[i].path+'</td>'
                    +'<td>'+left_context+'<b>'+core+'</b></td>'
                    +'<td>'+results[i].line_no+'</td>'
                    +'<td>'+this.range_drop_down(0,this.max_layer(results)+4,
                        Math.round(results[i].layer))+'</td>'
                    +'<td><a class="btn btn-danger" href="#" title="Delete row">'
                    +'<i class="fa fa-trash-o fa-lg"></i></a></td></tr>';
                rows += row;
                if ( results[i].layer == 0 )
                    $("#split_button").prop("disabled",true);
            }
            table.append('<tbody>'+rows+'</tbody>');
        }
        else
            $("#results-container").append('<p>No variant markup present!</p>');
        $("#results-container").append(this.format_original_xml(original));
        let o_pad = this.h_padding($("#original"));
        $("#original").width($("#original").width()-o_pad);
        if ( results.length > 0 ) {
            // set height initially to five rows
            let table_pos = Math.round($("#results-container").offset().top);
            let n_rows = 5;
            let ht_n_rows = this.measure_n_rows(n_rows);
            let res_ht = this.results_header_ht()+ht_n_rows;
            let orig_pos = Math.round($("#original").offset().top);
            let gap = orig_pos - (res_ht+table_pos);
            let remain_ht = $(window).height()-(table_pos+res_ht+gap);
            // adjust height of table so we can see the original text
            while ( remain_ht < ht_n_rows ) {
                n_rows--;
                ht_n_rows = this.measure_n_rows(n_rows);
                res_ht = this.results_header_ht(n_rows)+ht_n_rows;
                remain_ht = $(window).height()-(table_pos+res_ht+gap);
            }
            $("#results-wrapper").height(ht_n_rows);
            $("#original").height(remain_ht/*this.results_header_ht()+ht_n_rows*/);
            // set the width ofthe header cells
            $("#results-wrapper").css("top",$("#results-header th").outerHeight()+"px");
            for ( let i=0;i<5;i++ )
                this.set_header_cell_width(i);
        }
        $("#save_scan").remove();
        $("form div").append('<input type="button" value="save" id="save_scan"></input>');
        $("#save_scan").click(function(){
            let url = "/splitter/update";
            let obj = {};
            let aborted = false;
            obj.docid = self.projid;
            let templates = Array();
            $("#results tr").each(function(){
                let item = {};
                let l_td = $(this).children("td:eq(3)");
                let l_select = l_td.children("select");
                item.path = $(this).children("td:eq(0)").text();
                item.layer = parseInt(l_select.val());
                if ( item.layer == 0 ) {
                    alert("Set the layer number to more than 0 for "+item.path);
                    aborted = true;
                    return false;
                }
                templates.push(item);
            });
            if ( !aborted ) {
                obj.layers = JSON.stringify(templates);
                $.post(url,obj,
                    function(data){
                        $("#tabs-container").empty();
                        $("#results-container").empty();
                        if ( data.success ){
                            $("#"+self.target).append('<p id="error">Saved</p>');
                            $("#scan_button").remove();
                            $("#split_button").prop("disabled",false);
                        }
                        else
                            $("#"+self.target).append('<p id="error">Error: '+data.error+'</p>');
                    });
            }
        });
        // delete row
        $(".btn-danger").click(function(e){
            $(this).parents("tr").remove();
        });
    }
    install_handlers() {
        var self = this;
        $("#file_button").change(function(e){
            if ( $(this).val().length > 0 ){
                $("#scan_button").removeAttr("disabled");
                $("#split_button").removeAttr("disabled");
                $("#save_scan").remove();
                $("#save_split").remove();
            }
            else
                $("#scan_button").attr("disabled","disabled");
        });
        $("#scan_button").click(function(){
            $("form").attr("action","/splitter/scan");
        });
        $("#split_button").click(function(){
            $("form").attr("action","/splitter/split");
        });
        $("#response_frame").on("load",function(e){
            let html ="";
            let text = $(this).contents().text();
            if ( text.length>0 ) {
                let j_obj = JSON.parse(text);
                $("#error").remove();
                if ( j_obj.type=="scan" )
                    self.format_scan_results(j_obj.results,j_obj.original);
                else if ( j_obj.type == "split" ) {
                    //$("#"+self.target).append(html);
                    self.format_split_results(j_obj.results,j_obj.original);
                    $("#save_split").remove();
                    $("form div").append('<input type="submit" id="save_split" value="save"></button>');
                    $("#save_split").click(function(e){
                        $("form").attr("action","/splitter/layers/");
                    });
                }
                else if ( j_obj.hasOwnProperty("success") ) {
                    $("#tabs-container").empty();
                    $("#results-container").empty();
                    if ( j_obj.success )
                        $("#"+self.target).append('<p id="error">Saved</p>');
                    else
                        $("#"+self.target).append('<p id="error">Error: '+j_obj.error+'</p>');
                }
            }
        });
    }
    build_html() {
        let html ='<form target="response_frame" action="/splitter/scan" '
        +'method="post" enctype="multipart/form-data">'
        +'<div id="toolbar">XML file: '
        +'<input name="file_upload" id="file_button" type="file"></input>'
        +'<input type="submit" disabled="disabled" id="scan_button" value="scan"></input>'
        +'<input type="submit" disabled="disabled" id="split_button" value="split"></input>'
        +'<input type="hidden" name="docid" value="'+this.projid+'"></input></div>'
        +'</form>'
        +'<iframe id="response_frame" name="response_frame"></iframe>';
        let targ = $("#"+this.target);
        targ.append(html);
        targ.css("visibility","visible");
    }
}
