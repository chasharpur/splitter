/* requires ecdosis.js for getUrlParam */
function tabs(target,prefix,complete) {
    var self = this;
    this.complete = complete;
        
    /**
     * Build the left (home) menu
     * @param prefix the relative directory path of the website
     */
    this.buildMenuLeft = function(prefix) {
        var index = prefix.lastIndexOf("/");
        var lastItem = prefix.substring(index+1);
        // always show Home
        if ( lastItem == "Home" )
            lastItem = "";
        var sitePrefix = "";
        if ( index == -1 || lastItem.length==0 )
            sitePrefix = "";
        else
            sitePrefix = prefix.substring(0,index);
        if ( sitePrefix.length > 0 ) {
            $("#"+target).append('<table id="tabs-header-left"><tr></tr></table>');
            var url = '/ecdosis/menu?siteprefix='+sitePrefix;
            $.get(url,function(data) {
                for ( var i=0;i<data.items.length;i++ ) {
                    if ( data.items[i].name != lastItem ) {
                        if ( !data.items[i].hasOwnProperty("hidden") || !data.items[i].hidden ) {
                            var classNames;
                            if ( $("#tabs-header-left td").length==0 )
                                classNames = 'tab home';
                            else
                                classNames = 'tab hidden';
                            if ( $("#tabs-header-left td").length == 0 )
                                classNames += " leading";
                            else if ( i == data.items.length-1 )
                                classNames += " trailing";
                            else
                                classNames += " middle";
                            var link = '/'+data.items[i].name;
                            $("#tabs-header-left tr").append('<td class="'+classNames+'"><a href="'
                                +link+'">'+data.items[i].name+'</a></td>');
                        }
                    }
                }
                self.buildMenuRight(prefix);
            });
        }
        else
            this.buildMenuRight(prefix);
    };
    /**
     * Based on the url params wanted by a menu item, construct the query
     * @param item the menu item returned by the server
     * @return the query string for the menu item
     */
    this.getWantsParams = function(item) {
        var params = "";
        var wants = (item.hasOwnProperty("wants") && item.wants.length>0)?item.wants:"";
        if ( wants != undefined && wants.length > 0 ) {
            var parts = wants.split(" ");
            for ( var i=0;i<parts.length;i++ ) {
                var value = getUrlParam(parts[i]);
                if ( value != undefined && value.length > 0 ) {
                    params += (params.length == 0)?'?':'&';
                    params += parts[i]+'='+value;
                }
            }
        }
        return params;
    };
    /**
     * Build the sub-menu on the right
     * @param prefix the relative directory path to the submenu
     */
    this.buildMenuRight = function(prefix) {
        var pos = prefix.lastIndexOf("/");
        var subPrefix = (pos==-1)?"":prefix.substring(pos+1);
        $("#"+target).append('<table id="tabs-header-right"><tr></tr></table>');
        var url = '/ecdosis/menu?siteprefix='+prefix;
        $.get(url,function(data) {
            var first = true;
            if ( "items" in data ) {
                for ( var i=0;i<data.items.length;i++ ) {
                    var className = "";
                    if ( data.items[i].hasOwnProperty("hidden") && data.items[i].hidden )
                        className += 'hidden ';
                    if ( first ){
                        className += 'tab leading deselected';
                        first = false;
                    }
                    else if ( i == data.items.length-1 )
                        className += 'tab trailing selected';
                    else
                        className += 'tab middle deselected';
                    var link;
                    if ( subPrefix.length > 0 )
                        link = '/'+subPrefix+'/'+data.items[i].name;
                    else
                        link = "/"+data.items[i].name;
                    var params = self.getWantsParams(data.items[i]);
                    if ( params != undefined && params.length>0 )
                        link += params;
                    $("#tabs-header-right tr").append('<td class="'+className+'"><a href="'
                        +link+'">'+data.items[i].name+'</a></td>');
                }
                self.addHandlers();
                self.complete(self);
            }
            else
                alert ( data.error );
        });
    };
    /**
     * Select the named tab
     * @param name the name of the tab to foreground
     */
    this.selectTab = function(name) {
        var tabNo = this.getTabNo(name);
        var formerTab = $(".selected");
        formerTab.removeClass("selected");
        formerTab.addClass("deselected");
        var index = tabNo-1;
        var tab = $("#tabs-header-right td:eq("+index+")");
        tab.removeClass("deselected");
        tab.addClass("selected");
        if ( !tab.is(":visible") )
            tab.show();
        $(".tab-content:visible").hide();
        $("#tab-"+tabNo).show();
        //console.log("showing #tab-"+tabNo+" length="+$("#tab-"+tabNo).length);
    };
    /**
     * Add event handlers for the menus
     */
    this.addHandlers = function() {
        $("#tabs-header-left").mouseenter(function(){
            var hasRoom = $("#tabs-header-left").attr("data-hasroom");
            if ( hasRoom == "true" )
                $("#tabs-header-left td").show();
        });
        $("#tabs-header-left").mouseleave(function(){
            var hasRoom = $("#tabs-header-left").attr("data-hasroom");
            if ( hasRoom == "true" ) {
                $("#tabs-header-left td").each(function(){
                if ( !$(this).hasClass("home") )
                    $(this).hide();
                });
            }
        });
    };
    /**
    * Work out if there is room for the home menu
    */
    this.calcRoom = function() {
        var leftTableCopy = $("#tabs-header-left").clone();
        leftTableCopy.removeAttr("id");
        leftTableCopy.css("position","fixed");
        leftTableCopy.css("left","-1000px");
        leftTableCopy.appendTo("body");
        leftTableCopy.find("td").css("display","inline");
        var lhsWidth = leftTableCopy.width();
        var rhsWidth = $("#tabs-header-right").width();
        var headWidth = $("#header").width();
        if ( headWidth - (rhsWidth+lhsWidth) > 0 )
            $("#tabs-header-left").attr("data-hasroom","true");
        else
            $("#tabs-header-left").attr("data-hasroom","false");
        leftTableCopy.remove();
    }
    /**
     * Get the index of the tab with the given name
     * @param name the name to look for
     */
    this.getTabNo = function(name) {
        var tabNo = 1;
        var i = 1;
        $("#tabs-header-right td").each(function(){
            var tabText = $(this).text();
            //console.log(tabText);
            if ( tabText == name ){
                tabNo = i;
            }
            else
                i++;
        });
        return tabNo;
    };
    /**
     * Add the content of a tab but don't show it yet
     * @param target the target id
     * @param content the html content to add
     * @param name the name of the tab to add it to
     */
    this.addTabContent = function(target,content,name) {
        var tabNo = this.getTabNo(name);
        if ( $("#"+tabId).length==0 ) {
            $("#"+target).append('<div class="tab-content" id="tab-'+tabNo+'"></div>');
            var tabId = 'tab-'+tabNo;
            $("#"+tabId).append(content);
            $("#"+tabId).hide();
        }
    };
    this.buildMenuLeft(prefix);
}

