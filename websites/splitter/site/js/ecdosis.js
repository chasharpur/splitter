function getCssParam(obj,name) {
    var text = obj.css(name);
    if ( text.indexOf("px") != -1 ) {
        text = text.substring(0,text.indexOf("px"));
        if ( text.indexOf(".") != -1 )
            return parseFloat(text);
        else
            return parseInt(text);
    }
    else
        return 0;
}    
/**
 * Get a parameter from the current URL search path
 * @param name the name of the param to extract
 * @return its value or the empty string
 */
function getUrlParam(name) {
    var value = "";
    var search = window.location.search;
    if ( search.length > 0 ) {
        if ( search[0] == '?' )
            search = search.substring(1);
        var parts = search.split("&");
        for ( var i=0;i<parts.length;i++ ) {
            var pair = parts[i].split('=');
            if ( pair.length == 2 ) {
                if ( pair[0] == name ) {
                    value = pair[1];
                    break;
                }
            }
        }
    }
    return value;
}
/**
 * Set a parameter into the current search path
 * @param key the name of the param to set
 * @param value the value of the param
 */
function setUrlParam(key,value){
    var map = {};
    var search = window.location.search;
    if ( search.length > 0 ) {
        if ( search.indexOf("?")==0 )
            search = search.substring(1);
        var parts = search.split("&");
        for ( var i=0;i<parts.length;i++ ) {
            var halves = parts[i].split("=");
            map[halves[0]] = halves[1];
        }
    }
    map[key] = value;
    search = "?";
    for ( var k in map ) {
        if ( search.legth > 1 )
            search += "&";
        search += k;
        search += "=";
        search += map[k];        
    }
    var url = window.location.pathname+search;
    history.pushState({},"",url);
}
function emptyFooter() {
    return '<div class="footer"><p><span style="visibility:hidden">footer</span></p></div>';
}
function loginFits() {
    var lWidth = $("#login-group").width();
    var sWidth = $("#slogan").width();
    var logoWidth = $("#logo").width();
    var hWidth = $("#header").width();
    return lWidth+sWidth+logoWidth < hWidth;
}
/**
 * Do auto-resizing the hard way (no real browser support for this)
 * NB: We use a different resize function for the home page
 * @param resizeFunc an optional resize function
 */
function setResizeFunc(resizeFunc) {
    $(window).off("resize");
    if ( resizeFunc != undefined )
        $(window).resize(resizeFunc);
    else {
        $(window).resize(function(){
            var pWidth = $("#page-body").width();
            $("#header").width(pWidth);
            var offset = Math.round($("#header").outerHeight(true));
            $("#page-body").css("top",offset+"px");
        });
    }
}
/**
 * Get the default document for a given project
 * @param projid the projectid to get it for
 * @param callback call this function when you have it
 */
function getDefaultDocForProject(projid,callback) {
    $.get("/project/documents?projid="+projid,function(data) {
        if ( "documents" in data && data.documents.length > 0 ) {
            callback(data.documents[i].docid);
        }
        else
            callback("");
    });
}
/**
 * Get a default project
 * @param callback call this function with the project docid
 */
function getDefaultProject(callback) {
    $.get("/project/list",function(data){
        if ( data.length > 0 )
            callback(data[0].docid);
        else
            callback("");
    });
}
/**
 * Get a default document for an unspecified default project
 * @param callback call this function when you find it
 */
function getDefaultDoc(callback) {
    getDefaultProject(function(data){
        getDefaultDocForProject(data,callback);
    });
}
/**
 * Go to temporary full-screen
 */
function fullScreen() {
    this.preExpansion = {};
    this.preExpansion.pageMaxWidth = $("#page").css("max-width");
    this.preExpansion.pageWidth = $("#page").css("width");
    this.preExpansion.headerMaxWidth = $("#header").css("max-width");
    this.preExpansion.headerLeft = $("#header").css("left");
    $("#page").css("max-width","10000px");
    $("#page").css("width","100%");
    $("#header").css("left","0");
    $("#header").css("max-width","10000px");
}
/**
 * Exit temporary full-screen
 */
function escapeFullScreen() {
    this.preExpansion = {};
    $("#page").css("max-width",this.preExpansion.pageMaxWidth);
    $("#page").css("width",this.preExpansion.pageWidth);
    $("#header").css("max-width",this.preExpansion.headerMaxWidth);
    $("#header").css("left",this.preExpansion.headerLeft);
}
/**
 * Add a logo to the top of the page
 * @param tabs the menu tabs object
 * @param resizeFunc an optional resizing function to override default
 */
function addLogo(tabs,resizeFunc) {
    if ( $("#banner").length == 0 ) {
        $("#header").prepend('<div id="banner"></div>');
        $("#banner").append('<span id="logo">Ecdosis: </span><span id="slogan">Putting historical texts on the Web</span>');
        var search = location.search;
        if ( search[0] == "?" )
            search = "&"+search.substring(1);
        var offset = Math.round($("#header").outerHeight(true));
        $("#page-body").css("top",offset+"px");
        $("#header").width($("#page").width());
        if ( resizeFunc != undefined )
            setResizeFunc(resizeFunc);
        //tabs.calcRoom();
    }
}
function projectChooser(tabs,tabName,done){
    $.get("/project/list",function(data){
        if ( "success" in data && data.success ) {
            let projects = data.projects;
            var dialog = '<div id="choose_project"></div>';
            $("body").append(dialog);
            $("#choose_project").append('<p>Choose a project</p>');
            $("#choose_project").append('<select id="project_select"></select>');
            for ( var i=0;i<projects.length;i++ ){
                $("#project_select").append('<option value="'+projects[i].docid+'">'
                +projects[i].description+'</option>');
            }
            $("#choose_project").append('<input id="project_ok" value="OK"type="button"></input>');
            $("#choose_project").show();
            tabs.selectTab(tabName);
            addLogo(tabs);
            $("#project_ok").click(function(){
                docid = $("#project_select").val();
                $("#choose_project").remove();
                window.location.search ="?docid="+docid;
                done(docid);
            });
        }
        else
            alert("No projects");
    });
}
