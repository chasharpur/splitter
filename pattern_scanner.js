/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const sax_parser = require('./sax_parser');
const utils = require('./utils');
/**
 * Pattern splitter is a simplified layer-splitter for TEI-XML
 * Instead of DOM we use a linear SAX parse and record the <em>paths</em>
 * leading into each variant part. We maintain an index where these
 * paths are mapped to layer-names, e.g. /app/rdg@wit=X or /subst/del
 */
 class pattern_scanner extends sax_parser {
    /**
     * Constructor
     * @param config the JSON config is an array of objects. Each object
     * contains a "path" field, a layer number.
     * <p>A <em>path</em> is an XPath-like string describing the path of variant 
     * element measured from its <em>invariant</em> background. So 
     * "/subst/del/add" refers to the variant content of an add element inside 
     * a del, inside a subst. At any given level sibling elements are separated by :.
     * So /subst/del:add means an add after a del inside a subst.
     * path-components may also be qualified by an attribute name-value pair,
     * e.g. add@n=1 means "an add element with the attribute n=1".</p>
     * <p>The layer number specifies <em>local</em> state the text was left in 
     * by the author or scribe. So a correction of a correction is layer 3. The 
     * text's initial state is layer 1.</p>
     * <p>Splitter assigns all patterns to a layer number, however it is possible 
     * to split a text into versions by specifying a layer number for say abbrev 
     * or expan and then renaming the layer files as versions instead.</p>
     */    
    constructor(config) {
        super();
        this.config = config;
        /** current path to variant content */
        this.path;
        /** name of last sibling with a shared parent */
        this.last_sibling;
        /** splitting tags */
        this.splits = {};
        /** discriminators on siblings*/
        this.attributes = {};
        /** layers maps paths to layer-numbers (plus contexts, core, line_no if found)*/
        this.layers = {};
        /** the last popped element name */
        this.last;
        /** path after last stack pop (end-tag) */
        this.last_path;
        /**last component popped off at element end */
        this.last_popped_path = "";
        /** the text between innermost variant tags incl non-variant tags */
        this.current_core = "";
        /** the left-side context for the core text */
        this.left_context = "";
        /** we push the left context on entry to a variant element and pop when it ends */
        this.left_context_stack = [];
        /** for coming out of subst, app we save the last valid context */
        this.saved_left_context = "";
        /** load the layers */
        this.load_config(config);
    }
    /**
     * Load the config, which contains the layers we known about in advance
     * @param conf a json array described above
     */
    load_config( conf ) {
        for ( let i=0;i<conf.length;i++ ){
            let j_obj = conf[i];
            let j_path = j_obj.path;
            let spec = {};
            spec.layer = j_obj.layer;
            this.layers[j_path] = spec;
        }
    }
    /**
     * Scan an TEI-XML file for patterns of variant tags (ignoring others)
     * @param tei the TEI content containing variant markup
     * @return an array of found patterns
     */
    scan( tei ) {
        this.line_no = 1;
        this.splits = {};
        this.attributes = {};
        // xpath like expression to current element
        this.path = "";
        // last path used on previous variant element (to identify siblings)
        this.last_path = "";
        // hard-wire config for now
        this.attributes.rdg = "wit";
        this.attributes.lem = "wit";
        this.splits.add = true;
        this.splits.del = true;
        this.splits.abbrev = true;
        this.splits.expan = true;
        this.splits.rdg = true;
        this.splits.lem = true;
        this.splits.app = true;
        this.splits.mod = true;
        this.splits.choice = true;
        this.splits.subst = true;
        // add other variant tags here but they must either nest or have siblings
        // variant elements linking to others can't be processed by this method
        // so expan, abbrev, sic, corr etc but their content will be assigned 
        // to layers when they are in fact versions
        this.parse(tei);
        return this.layers_to_json();
    }
    /**
     * Literally sort two objects on their path fields.
     * @param a the first object
     * @param b the second object
     * @return 0 if they are equal, else 1 if s &gt; b else -1
     */
    compare( a, b ) {
        let stra = a.path;
        let strb = b.path;
        return stra.localeCompare(strb);
    }
    /**
     * Sort an array of path objects
     * @param j_arr the array of path json objects
     * @return the sorted array
     */
    sort( j_arr ) {
        let increment = Math.floor(j_arr.length / 2);
        while (increment > 0) {
            for (let i = increment; i < j_arr.length; i++) {
                let j = i;
                let temp = j_arr[i];
                while (j >= increment 
                    && this.compare(j_arr[j-increment],temp)>0 ) {
                    j_arr[j] = j_arr[j-increment];
                    j = j - increment;
                }
                j_arr[j] = temp;
            }
            if (increment == 2) 
                increment = 1;
            else
                increment *= Math.floor(5.0 / 11);
        }
        return j_arr;
    }
    /**
     * Pik out the found patterns from layers hash and put them in an array
     * @return a JSONArray of objects containing path, line_no, left_context and core 
     */
    layers_to_json() {
        let keys = Object.keys(this.layers);
        let arr = Array();
        for ( let key of keys ) {
            let j_obj = this.layers[key];
            if ( j_obj.hasOwnProperty("left_context") )  {
                j_obj.path = key;
                arr.push(j_obj);
            }
        }
        if ( arr.length==0 ) {
            let j_obj = {};
            j_obj.layer = 1;
            j_obj.path = "";
            arr.push( j_obj );
        }
        // sort them into a good order by path
        return this.sort(arr);
    }
    /**
     * We encountered some text between tags
     * @param text the text we found
     */
    characters(text){
        // we can't start the core with whitespace
        if ( this.current_core.length>0 || !utils.is_whitespace(text) )
            this.current_core += text;
        this.inc_left_context(text);
    }
    /**
     * We encountered a start-element in the XML
     * @param name the name of the element
     * @param attrs an array of {name:...,value:...} pairs
     */
    start_element(name,attrs){
        if ( this.splits.hasOwnProperty(name) ){
            this.push_left_context();
            if ( this.is_sibling() ) 
                this.path = this.last_path + ":";
            else
                this.path += "/";
            this.path += this.get_name(name,attrs);
            this.current_core = "";
        }
        else if ( this.current_core.length>0 ) {
            this.current_core += this.build_start_tag(name,attrs);
        }
    }
    /**
     * We encountered end-element in the XML
     * @param name the name of that element
     */
    end_element(name){
        if ( this.splits.hasOwnProperty(name) ){
            let prev_context = this.left_context;
            this.pop_left_context();
            if ( prev_context.length > this.left_context.length )
                this.saved_left_context = prev_context;
            if ( this.current_core.length>0 )
                this.save_path();
            else
                this.left_context = this.saved_left_context;
            this.current_core = "";
            // save previous path to identify siblings
            this.last_path = this.path;
            let index = this.path.lastIndexOf("/");
            if ( index != -1 )
                this.path = this.path.substring(0,index);
        }
        else if ( this.current_core.length>0 ) {
            this.current_core += this.build_end_tag(name);
        }
    }
    /**
     * Is there an ancestor element of the given name?
     * @param component the ancestor
     * @return true if it is else false
     */
     has_ancestor( component ) {
        let current_path = this.path;
        let parts = current_path.split("/");
        for ( let i=parts.length-1;i>=0;i-- )
            if ( this.get_last_path(parts[i]) == component )
                return true;
        return false;
    }
    /**
     * Get the last sibling of the path component
     * @param component the full path-component between slashes
     * @return the last part of that if subdivided into siblings
     */
     get_last_path( component ) {
        if ( component.indexOf(":")!= -1 ) {
            let siblings = component.split(":");
            return siblings[siblings.length-1];
        }
        else
            return component;
    }
    /**
     * Save the current left context on entry to a variant element
     */
    push_left_context(){
        let new_left_context = this.left_context.repeat(1);
        this.left_context_stack.push(this.left_context);
        this.left_context = new_left_context;
    }
    /**
     * Restore the left context to what it was on entry to this element
     */
    pop_left_context(){
        this.left_context = this.left_context_stack.pop();
    }
    /**
     * Get the name of the element
     * @param name the bare element's name
     * @param attrs an array of name, value pairs
     * @returns the name with a possible attribute annotation
     */
    get_name(name,attrs){
        let attr_name = this.attributes[name];
        if ( this.attributes.hasOwnProperty("name")&&this.has_attr(attrs,attr_name) ) {
            return name+"@"+this.attributes[name]+"="+this.get_attr(attrs,attr_name);
        }
        else
            return name;
    }
    /**
     * Does a start-element have a given attribute?
     * @param atts the attributes of hte start element (name,value pair array)
     * @param attr_name the attribute name to test for
     * @returns true or false
     */
    has_attr(atts,attr_name){
        for ( let att of atts ) {
            if ( att.name == attr_name )
                return true;
        }
        return false;
    }
    /**
     * Get a known attribute value
     * @param atts array of name,value pairs
     * @param name the name of the attribute whose value is wanted
     * @returns the attribute value
     */
    get_attr(atts,name){
        for ( let att of atts ) {
            if ( att.name == name )
                return att.value;
        }
        return "";
    }
    /**
     * Get the last part of the current path
     * @return the last child of the path
     */
    ultimate_path() {
        let parts = this.path.split("/");
        if ( parts.length > 1 )
            return this.get_last_path(parts[parts.length-1]);
        else
            return "";
    }
    /**
     * Add text to the left context and maintain it at a maximum length
     * @param str some new text detected by this.characters
     */
    inc_left_context(str){
        this.left_context += str;
        while ( this.left_context.length > 50 || /^[,.‘”!? ]/.test(this.left_context)) {
            let sp_index = this.left_context.indexOf(" ");
            let nl_index = this.left_context.indexOf('\n');
            let index = Math.max(sp_index,nl_index);
            if ( index != -1 ){
                this.left_context = this.left_context.substring(index+1);
            }
            else
                break;
        }
    }
    /**
     * Is the current tag a sibling of the preceding one?
     * @returns true if it is a sibling
     */
    is_sibling() {
        let index = this.last_path.lastIndexOf("/");
        if ( index != -1 ) {
            let last_path_bare = this.last_path.substring(0,index);
            return last_path_bare == this.path && this.path.length > 0;
        }
        return false;
    }
    /**
     * Save the current path (and core, line_no etc) to this.layers hash
     */
    save_path(){
        if ( this.layers.hasOwnProperty(this.path) ) {
            let j_obj = this.layers[this.path];
            if ( !j_obj.hasOwnProperty("left_context") ){
                j_obj.left_context = this.left_context;
                j_obj.core = this.current_core;
                j_obj.line_no = this.line_no;
            }
        }
        else {
            let j_obj = {};
            j_obj.left_context = this.left_context;
            j_obj.core = this.current_core;
            j_obj.line_no = this.line_no;
            j_obj.layer = 0;
            this.layers[this.path] = j_obj;
        }
    }
}
module.exports = pattern_scanner;
