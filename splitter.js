/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const pattern_scanner = require('./pattern_scanner');
const sax_splitter = require('./sax_splitter');
const utils = require('./utils');
const path = require('path');
const fs = require('fs');
/**
 *  Split an xml file into layers based on patterns
 */
class splitter {
    /**
     * Read a splitter config
     * @param root the ecdosis root
     * @param docid the docid for the xml file
     */
    static read_config(root,docid) {
        let splitter_id = "splitter/"+docid;
        let j_str = null;
        while ( j_str == null && splitter_id.length>0 ) {
            let splitter_path = path.join(root,"config",splitter_id,"config.json");
            if ( fs.existsSync(splitter_path) ) {
                j_str = fs.readFileSync(splitter_path,{encoding:"utf8"});
            }
            if ( j_str == null ) {
                let default_splitter_path = path.join(utils.chomp(splitter_path),"default","config.json");
                if ( fs.existsSync(default_splitter_path) ) {
                    j_str = fs.readFileSync(default_splitter_path,{encoding:"utf8"});
                }
            }
            if ( j_str == null )
                splitter_id = utils.chomp(splitter_id);
        }
        if (j_str==null)
            throw "splitter config for "+docid+" not found";
        else {
            let j_obj = JSON.parse(j_str);
            if ( j_obj.hasOwnProperty("layers") )
                return j_obj.layers;
            else
                throw "missing layer keyword in splitter config";
        }
    }
    /**
     * Scan an XML file for patterns
     * @param root the ecdosis root dir
     * @param post the parsed post parameters
     */
    static scan(root,post) {
        let j_obj = {};
        if ( "file_upload" in post.files ) {
            if ( utils.ends_with(post.files.file_upload.name,".xml")) {
                if ( "docid" in post ) {
                    let xml = fs.readFileSync(post.files.file_upload.path,{encoding:"utf8"});
                    let config = [];
                    try {
                        config = splitter.read_config(root,post.docid);
                    }
                    catch ( err ) {
                        console.log(err);
                    }
                    let ps = new pattern_scanner(config);
                    j_obj.type = "scan";
                    let j_arr = ps.scan( xml );
                    j_obj.results = j_arr;
                    j_obj.original = xml;
                }
                else
                    throw "missing docid parameter";
            }
            else
                throw "not an xml file";
        }
        else
            throw "no file uploaded";
        return j_obj;
    }
    /**
     * Split an XML file into layers
     * @param root the ecdosis root dir
     * @param post the parsed post parameters
     */
    static split(root,post) {
        let j_obj = {};
        if ( "file_upload" in post.files 
            && utils.ends_with(post.files.file_upload.name,".xml") 
            && "docid" in post ) {
            let xml = fs.readFileSync(post.files.file_upload.path,{encoding:"utf8"});
            let config = splitter.read_config(root,post.docid);
            let ss = new sax_splitter(config);
            ss.scan_for_layers( xml );
            ss.split( xml );
            let j_arr = Array();
            let split_layers = ss.get_layers();
            for ( let i=0;i<split_layers.length;i++ )
                j_arr.push( split_layers[i].to_string() );
            j_obj.type = "split";
            j_obj.results = j_arr;
            j_obj.original = xml;
        }
        else {
            if ( ! "file_upload" in post.files )
                throw "No xml file specified";
            else if ( ! "docid" in post )
                throw "missing docid";
            else
                throw post.files.file_upload.name+" is not an xml file";
        }        
        return j_obj;
    }
    /**
     * Save an updaed batch of templates and layer-numbers to disk
     * @param root the ecdosis root dir
     * @param post the post parameters parsed
     */
    static update(root,post) {
        if ( "layers" in post && "docid" in post ) {
            let config_id = "splitter/"+post.docid;
            let config_path = path.join(root,"config",config_id,"config.json");
            let mymap = {};
            if ( fs.existsSync(config_path) ){
                let j_str = fs.readFileSync(config_path,{encoding:"utf8"});
                let j_obj = JSON.parse(j_str);
                let old_layers = j_obj.layers;
                for ( let i=0;i<old_layers.length;i++ ) {
                    let item = old_layers[i];
                    mymap[item.path] = item;
                }
            }
            // mix in the new values
            let layers = JSON.parse(post.layers);
            for ( let i=0;i<layers.length;i++ ) {
                let item = layers[i];
                mymap[item.path] = item;
            }
            // reserialise back as array
            let j_arr = Array();
            let keys = Object.keys(mymap);
            for ( let key of keys ) {
                j_arr.push(mymap[key]);
            }
            // rebuild database object
            let bson = {};
            bson.layers = j_arr;
            fs.writeFileSync(config_path,JSON.stringify(bson));
            return {layers:layers.length};
        }
        else
            throw "missing layers or docid parameter";
    }
    static version_id( file ) {
        if ( file.indexOf("#")!=-1 )
            file = file.substring(file.indexOf("#")+1 );
        if ( file.lastIndexOf(".") != -1 )
            file = file.substring(0,file.lastIndexOf("."));
        if ( file.lastIndexOf("-fixed") != -1 )
            file = file.substring(0,file.lastIndexOf("-fixed"));
        if ( file.lastIndexOf("-flattened") != -1 )
            file = file.substring(0,file.lastIndexOf("-flattened"));
        return file;
    }
    static document_id( file ) {
        let v_id = splitter.version_id(file);
        if ( utils.is_lowercase(v_id.charAt(v_id.length-1)) )
            return v_id.substring(0,v_id.length-1);
        return v_id;        
    }
    static get_default_section( root, projid ) {
        let proj_path = path.join(root,"projects",projid,"project.json");
        if (fs.existsSync(proj_path)) {
            let j_str = fs.readFileSync(proj_path,{encoding:"utf8"});
            let j_obj = JSON.parse(j_str);
            if ( j_obj.hasOwnProperty("sections")) {
                let j_arr = j_obj.sections;
                if ( j_arr.length>0 )
                    return j_arr[0];
            }
        }
        return null;
    }
    /**
     * Save the layers of a split XML file (section param optional)
     * @param root the ecdosis root directory
     * @param post the parsed post parameters
     */
    static save_layers(root,post) {
        if ( "xml_file" in post.files 
            && utils.ends_with(post.files.file_upload.name,".xml") ) {
            let j_obj = {n_layers:0};
            let config = splitter.read_config(root,post.docid);
            let xml = fs.readFileSync(post.files.file_upload.path,{encoding:"utf8"});
            // we have to repeat the split because it was not saved
            let ss = new sax_splitter(config);
            ss.scan_for_layers( xml );
            ss.split( xml );
            let split_layers = ss.get_layers();
            let doc_id = splitter.document_id( post.files.file_upload.name );
            let v_id = splitter.version_id( post.files.file_upload.name );
            let file_id;
            let section = null;
            if ( "section" in post )
                section = post.section;
            else
                section = splitter.get_default_section(root,post.docid);
            if ( section == null )
                file_id = path.join(post.docid,doc_id);
            else
                file_id = path.join(post.docid,section,doc_id);
            for ( let i=0;i<split_layers.length;i++ ) {
                let l = split_layers[i];
                let l_name = "layer-"+(i+1);
                if ( i == split_layers.length-1 )
                    l_name = "layer-final";
                let layer_path = path.join(root,"layers",file_id,v_id,l_name,"layer.xml");
                //j_obj.text = l.text;
                //j_obj.format = "application/xml";
                j_obj.n_layers++;
                fs.writeFileSync(layer_path,l.text);
            }
            return j_obj;
        }
    }
}
module.exports = splitter;
