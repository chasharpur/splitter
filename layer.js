/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const utils = require('./utils');
/**
 *A storage object for layers with the ability to get the last segment
 */
class layer {
    constructor() {
        this.text = "";
        this.segments = Array();
    }
    /**
     * Add some text (plain or XML) to the layer
     * @param stuff the stuff to add
     */
    add( stuff ) {
        // remove double spaces
        if ( utils.starts_with(stuff," ") ) {
            let len = this.text.length;
            while ( len > 0 && this.text.charAt(len-1) == ' ' ) {
                this.text = this.text.substring(0,len-1);
                len = this.text.length;
            }
        }
        this.text += stuff;
        this.segments.push( stuff);
    }
    /**
     * Get the last added segment
     * @return a String of XML/text
     */
    last() {
        if ( this.segments.length > 0 )
            return this.segments[this.segments.length-1];
        else
            return "";
    }
    to_string() {
        return this.text;
    }
}
module.exports = layer;
