/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const utils = require('./utils');
const path = require('path');
const fs = require('fs');
class project {
    /**
     * List all projects in the system
     * @param root ecdosis root dir
     * @param q the parsed get params
     * @return a simple object with a projects field
     */
     static list_projects(root,q) {
        let list = utils.list_resources_sync(root,"projects","",["project.json"]);
        let projects = Array();
        for ( let file of list ) {
            let j_str = fs.readFileSync(file);
            let j_obj = JSON.parse(j_str);
            let projid = utils.docid_from_path(file,"projects",["project.json"]);
            let icon_path = path.join(root,"corpix",projid,"project","icon");
            if ( fs.existsSync(icon_path))
                j_obj.icon = icon_path.substring(icon_path.indexOf("corpix")-1);
            //let username = os.userInfo().username;
            //if ( j_obj.hasOwnProperty("owner") && j_obj.owner == username )
            // allow anyone to edit
                j_obj.editable = true;
            j_obj.language = utils.language_from_projid(projid).language;
            j_obj.docid = projid;
            projects.push(j_obj);
        }
        return {projects:projects};
    }
}
module.exports = project;
