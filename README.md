# Splitter 

Splitter is a program for splitting TEI-XML files into layers. Layers 
are separate files representing temporal states of change to a text on 
a local level. The TEI tags recognised are <add>, <del>, <subst>, <mod>, 
<app>, <rdg>, <lem>, <abbrev>, <expan>, <choice>. The split files do not 
contain any of these tags, rather they are used to determine where to 
split the files, and which bits of text go into which layer.

Splitter can do two things. First it can scan a TEI-XML file for 
patterns of these tags. The user then has the opportunity to assign
layer numbers to the contents of these patterns. When you have assigned 
all unassigned patterns click "save", which will remember these patterns 
for next time. Unless you save you cannot split.

Splitter can these use these patterns to split the file into layers. 
Layers contain all the markup of the source except for the variant tags 
used to split. Any attributes on the variant tags are lost if they are 
not used in the split.

Layers are needed to ensure interoperability of the TEI-XML data by 
ensuring that the text is always in conceptual order and all parallel 
constructs are removed. For this to work, however, the XML must be coded 
temporally. Normally TEI files are encoded topographically, that is, 
recording the editing actions on the page. Temporal encoding records the 
states of the text that result from these changes. If the changes are 
simple then temporal and topographic encodings are likely to be the same. 
In more complex cases, however, it will quickly become clear that the 
topographic encoding cannot be successfully split into temporal layters. In 
this case the complex revision sites must be re-encoded temporally.

# Installation
Splitter is a Node.js program. To run it first install Node.js. Then 
install the dependencies:

    npm install formidable

To run it on Linux or MacOS open the Terminal program and navigate to 
the splitter folder via cd. Then type:

    ./start.sh

It is unlikely, but if port 8082 is already in use an EADDRINUSE is issued 
by the shell. If the service on port 8082 is Splitter itself you can't run 
it twice. If it is something else then configure Splitter to use another 
port in start.sh. Change the line

    node main.js -e ./ecdosis-data -w ./websites &

to

    node main.js -e ./ecdosis-data -w ./websites -p 8083 &

to switch to port 8083 or some other port.

To stop it on Linux or MacOS:

    ./stop.sh

Once it is running open a browser and navigate to http://localhost:8082 (or 
the other port you defined above) to use Splitter. If no project was 
specified in the URL a project selector is displayed. This project will be 
used to store the configuration and the output layers. Splitter works best 
if you choose a project corresponding to the examples you wish to run.

Once a project is selected, click the 'Browse...' button to load an XML 
file from the examples folder or load your own TEI-XML file. Now press 
the 'scan' button. All discovered patterns involving variant tags will 
be displayed. If there are any patterns it does not understand these 
will be flagged and assigned layer 0, which is not allowed. Splitter 
comes with a large number of predefined patterns but it is possible for 
new patterns to occur. You must assign those patterns to a valid layer 
and save your changes before you can split. As a general guide the base 
text is layer 1 and changes to it are layer 2 etc. Assess the content 
of the pattern as belonging to a level of correction that corresponds 
to a number counting from the baseline text.

To split a file into layers click the 'split' button. In the split 
display you can compare the original with each of the split layers in 
turn by clicking on their tabs. Line numbers will not correspond precisely.

After the split, if the result is not satisfactory edit the source XML 
file to correct the problem, e.g. by temporally re-encoding the topographic 
enncoding. Temporal encoding follows the pattern:

    <subst><del>...</del><del>...</del><add>...</add></subst>, or
    <app><rdg>...</rdg><rdg>...</rdg><rdg>...</rdg></app>

although any pattern can be used so long as it can be unambiguously 
assigned to a layer, i.e. if everywhere it occurs it always represents 
text in that layer. The file must then be reloaded via 'Browse...' for the 
new XML to be scanned or split.

Due to the habit of most transcribers of using significant whitespace 
around variant tags most files will need *some* editing.

# Patterns
Patterns follow the XPath syntax very basically. The pattern 
/subst/add/del means a <del> inside an <add> inside a <subst>, whereas 
/subst/add:del means a <del> following an <add> inside a <subst>. So "/" 
means go down a level in the hierachy and : means go horizontally, 
specifying a sibling at that level. So /app/rdg:rdg:rdg:rdg means the 
fourth <rdg> in an <app>.

# Output
If saved the split layers go into ecdosis-data/layers. The scan config 
is saved in ecdosis-data/config/splitter. The actual data is stored in 
a folder reflecting a project path relative to one of those two folders.
