/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const utils = require("./utils");

class sax_parser {
    constructor() {
        // position of next character to read
        this.src_position = 0;
        // position of last tag-end or tag-start
        this.tag_position = 0;
        /** original xml to parse */
        this.xml = null;
        this.line_no = 1;
    }
    report_error(token) {
        this.error("unexpected token "+token+" on line "+this.line_no);
        return 0;
    }
    get_att_value(atts,name) {
        for ( let a of atts ) {
            if ( a.name == name )
                return a.value;
        }
        return null;
    }
    build_end_tag(name) {
        return '</'+name+'>';
    }
    /**
     * Adjust curr_position
     */
    check_tag_position() {
        let index = this.xml.indexOf(">",this.tag_position );
        if ( index != -1 )
            this.tag_position = index+1;
    }
    verify_curr_position( tag ) {
        let state = 0;
        let matched = 0;
        for ( let i=this.curr_position;i<this.xml.length;i++ ) {
            let c = this.xml.charAt(i);
            switch ( state ) {
                case 0:
                    if ( c == '<')
                        state =1;
                    else
                        state = -1;
                    break;
                case 1: // seen '<'
                    if ( c == '/' )
                        state = 2;
                    else
                    {
                        if ( tag.charAt(matched) == c )
                        {
                            matched++;
                            if ( matched == tag.length )
                                state = 3;
                        }
                        else
                            state = -1;
                    }
                    break;
                case 3:
                    if ( c == '>' || c==' ' )
                        state = 4;
                    else
                        state = -1;
                    break;
            }
            if ( state == -1 || state > 3 )
                break;
        }
        if ( state == -1 )
            console.log("Failed to verify curr_position "+this.curr_position);
    }
    build_start_tag(name,atts) {
        let start_tag = '<'+name;
        for ( let attr of atts ){
            start_tag += ' ';
            start_tag += attr.name;
            start_tag += '="';
            start_tag += attr.value;
            start_tag += '"';
        }
        start_tag += '>';
        return start_tag;
    }
    /**
     * Parse some xml
     * @param xml some xml, preferably syntactically correct
     */
    parse(xml) {
        let state = 0;
        let tag_name = "";
        let attr_name = "";
        let attr_value = "";
        let entity_name = "";
        let text = "";
        let stack = Array();
        let attrs = Array();
        this.line_no = 1;
        for ( this.src_postion=0;this.src_position<xml.length;this.src_position++ ) {
            let token = xml[this.src_position];
            switch ( state ) {
                case 0: // looking for '<'
                    if ( token == '<' ){
                        if ( text.length>0 ) {
                            this.characters(text);
                            text = "";
                        }
                        attrs = Array();
                        state = 1;
                    }
                    else if ( token == '&' ){
                        entity_name = "";
                        if ( text.length > 0 ) {
                            this.characters(text);
                            text = "";
                        }
                        state = 7;
                    }
                    else {
                        if ( token == '\n' )
                            this.line_no++;
                        text += token;
                    }
                    break;
                case 1: // seen '<'
                    if ( token == '/' ) {
                        tag_name = "";
                        state = 2;
                    }
                    else if ( utils.is_letter(token) ) {
                        tag_name = token;
                        state = 3;
                    }
                    else if ( token == '?' )
                        state = 8;
                    else
                        state = this.report_error(token);
                    break;
                case 2: // end-tag
                    if ( utils.is_letter(token)||utils.is_number(token) )
                        tag_name += token;
                    else if ( token == '>' ) {
                        if ( stack.length==0 || stack[stack.length-1] != tag_name ) 
                            this.error("syntax error: unexpect end-tag "+tag_name+" one line "+this.line_no );
                        else {
                            stack.pop();
                            this.end_element(tag_name);
                        }
                        state = 0;
                    }
                    else  
                        state = this.report_error(token);
                    break;
                case 3: // start-tag
                    if ( utils.is_letter(token)||utils.is_number(token) )
                        tag_name += token;
                    else if ( utils.is_whitespace(token) ){
                        if ( token == '\n' )
                            this.line_no++;
                        state = 4;
                    }
                    else if ( token == '>' ) {
                        stack.push(tag_name);
                        this.start_element(tag_name,attrs);
                        state = 0;
                    }
                    else
                        state = this.report_error(token);
                    break;
                case 4: // attribute-name
                    if ( utils.is_letter(token)||utils.is_number(token) )
                        attr_name += token;
                    else if ( token == '=' )
                        state = 5;
                    else if ( utils.is_whitespace(token) && attr_name.length==0 )
                        continue;
                    else if ( token == '>' ){
                        if ( tag_name.length>0 ) {
                            stack.push(tag_name);
                            this.start_element(tag_name,attrs);
                            state = 0;
                        }
                        else {
                            this.error("empty tag name on line "+this.line_no);
                            state = 0;
                        }
                    }
                    else if ( token == '/' ) 
                        state = 9;
                    else
                        state = this.report_error(token);
                    break;
                case 5: // looking for double-quote
                    if ( token == '"' )
                        state = 6;
                    else 
                        state = this.report_error(token);
                    break;
                case 6: // attribute-value
                    if ( token == '"' ) {
                        attrs.push({name:attr_name,value:attr_value});
                        attr_name = "";
                        attr_value = "";
                        state = 4;
                    }
                    else
                        attr_value += token;
                    break;
                case 7: // seen &
                    if ( utils.is_letter(token) || utils.is_number(token) )
                        entity_name += token;
                    else if ( token == ';' ){
                        this.entity(entity_name);
                        state = 0;
                    }
                    else {
                        this.error("invalid entity on line "+this.line_no);
                        state = 0;
                    }
                    break;
                case 8: // processor
                    if ( token == '>' )
                        state = 0;
                    break;
                case 9: // seen <tag atts/
                    if ( token == '>' ) {
                        this.start_element(tag_name,attrs);
                        this.end_element(tag_name);
                        state = 0;
                    }      
                    else
                        state = this.report_error(token);  
                    break;                
            }
        }
        if ( text.length>0 )
            this.characters(text);
    }
    characters(text){
    }
    start_element(name,attrs){
    }
    end_element(name){
    }
    error(message){
    }
    entity(name) {
        if ( name == "amp" )
            this.body += '&';
        else if ( name == 'lt' )
            this.body += '<';
        else if ( name == 'gt' )
            this.body += '>';
        else
            console.log("unhandled entity "+name);
    }
}
module.exports = sax_parser;
