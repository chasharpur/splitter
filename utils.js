/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const fs = require('fs');
const path = require('path');
class utils {
    static is_whitespace(str) {
        var whitespaces = " \n\r\t";
        for ( var i=0;i<str.length;i++ ) {
            if ( whitespaces.indexOf(str[i]) == -1 )
                return false;
        }
        return true;
    }
    static is_letter(str){
        return str.toUpperCase() != str.toLowerCase();
    }
    static is_number(str) {
        for ( var i=0;i<str.length;i++ ) {
            if ( str.charCodeAt(i) <48 || str.charCodeAt(i) >57  )
                return false;
        }
        return true;
    }
    static ends_with(str, suffix) {
        if ( str.length >= suffix.length ) {
            let str_suffix = str.substring(str.length-suffix.length);
            return str_suffix == suffix;
        }
        else
            return false;
    }
    static starts_with(str,prefix) {
        return prefix.length<=str.length&&str.substring(0,prefix.length)==prefix;
    }
    static is_lowercase(str){
        var answer = true;
        for ( var i=0;i<str.length;i++ )
        {
            var token = str.charAt(i);
            if (token.toUpperCase()==token||utils.is_number(token) )
                return false;
        }
        return answer;
    }
    /**
     * Remove the rightmost segment of the path and resource
     * @param file_path the file path to chomp
     * @return the remains of the path
     */
    static chomp( file_path ) {
        var popped = "";
        file_path = utils.normalise_path(file_path);
        var index = file_path.lastIndexOf( "/" );
        if ( index != -1 )
            popped = file_path.substring( 0, index );
        return popped;
    }
    static normalise_path(res){
        if ( res.indexOf("\\") !=-1 )
            return res.replace(/\\/g,"/");
        else
            return res;
    }
    /**
     * list resources of a particular type
     * @param root the root directory
     * @param section the section within root
     * @param docid the resource docid
     * @param fname an array of file names to look for
     * @return an array of file paths
     */
    static list_resources_sync(root,section,docid,fnames) {
        var dir;
        if ( docid.length==0 )
            dir = path.join(root,section);
        else
            dir = path.join(root,section,docid);
        let list = Array();
        if ( fs.existsSync(dir) )
            utils.list_files_in_dir(dir,fnames,list);
        return list;
    }
    /**
     * List files in a directory recursively
     * @param dir the directory path
     * @param fnames an array of files to seek or null
     * @param list the list to store them in
     */
    static list_files_in_dir(dir,fnames,list){
        let files = fs.readdirSync(dir);
        for ( let file of files ){
            let full_path = path.join(dir,file);
            let stats = fs.statSync(full_path);
            if ( stats.isDirectory() )
                utils.list_files_in_dir(full_path,fnames,list);
            else if ( fnames==null )
                list.push(full_path);
            else {
                for ( let filename of fnames ){
                    if ( filename == file )
                        list.push(full_path);
                }
            }
        }
    }
    /**
     * Extract the docid from a file path 
     * @param file_path the full path to the file
     * @param section the section after which the docid begins
     * @param fnames an array of possible file-names to end the path
     */
     static docid_from_path(file_path,section,fnames) {
        // only 1st part of section name is valid
        section = utils.normalise_path(section);
        file_path = utils.normalise_path(file_path);
        if ( section.indexOf("/") != -1 )
            section = section.substring(0,section.indexOf("/"));
        var prefix = file_path.substring(file_path.indexOf(section)+section.length+1);
        for ( var fname of fnames ) {
            if ( prefix.lastIndexOf(fname) == prefix.length-fname.length ) {
                prefix = prefix.substring(0,prefix.length-(fname.length+1));
                break;
            }
        }
        return prefix;
    } 
    /**
     * Get the language from a project id
     * @return {code:<2-letter-code>,language:<name of language>}
     */
    static language_from_projid(projid) {
        projid = utils.normalise_path(projid);
        let parts = projid.split("/");
        if ( parts.length==2 && utils.languages.hasOwnProperty(parts[0]) )
            return utils.languages[parts[0]];
        else
            return {code:'en',language:"English"};
    }
}
utils.languages = {italiano:{code:'it',language:'Italiano'},english:{code:'en',language:'English'}};
module.exports = utils;
