/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const formidable = require('formidable');
const http = require('http');
const url = require('url');
const qs = require('querystring');
const fs = require('fs');
const path = require('path');
const menu_cache = require('./menu');
const splitter = require('./splitter');
const utils = require('./utils');
const project = require('./project');

class ecdosis {
    static print_json_success(res,obj){
        if ( obj == null )
            obj = {};
        obj.success = true;
        let str = JSON.stringify(obj);
        res.setHeader('content-type', 'application/json');
        res.status = 200;
        res.write(str);
        return res.end();
    }
    static get_ecdosis_version(root,q) {
        return {version:ecdosis.version};
    }
    /**
     * Print an error message in html format as a complete web page
     * @param res the response
     * @param message the message to send
     * @param status the status to set
     */
    static print_json_error(res,err,status) {
        res.setHeader('content-type', 'application/json');
        res.status = status;
        if ( err != null ) {
            var message = (err.hasOwnProperty("message"))?err.message:err;
            var lines = ( err.hasOwnProperty("stack") )?err.stack.split("\n"):[];
            var stack = lines.join("<br>");
            var obj = {"success":false,"error":message,"stack":stack};
            res.write(JSON.stringify(obj));
            console.log(message);
        }
        else
            res.write('{"success":false","error":"error"}');
        return res.end();
    }
    static handle_post(post,action,res) {
        try {
            let obj = action(ecdosis.root,post);
            if ( obj==undefined )
                obj = null;
            ecdosis.print_json_success(res,obj);
        }
        catch ( err ) {
            return ecdosis.print_json_error(res,err,500);
        }
    }
    static handle_get(q,action,res) {
        try {
            if ( "docid" in q )
                q.docid = utils.normalise_path(q.docid);
            if ( "version1" in q )
                q.version1 = utils.normalise_path(q.version1);
            q.web_root = ecdosis.web_root;
            q.res = res;// used to download images
            let obj = action(ecdosis.root,q);
            if ( obj == undefined )
                obj = null
            if ( (! "skip_response" in q) || !q.skip_response )
                ecdosis.print_json_success(res,obj);
        }
        catch ( err ) {
            return ecdosis.print_json_error(res,err,500);
        }
    }
    static handle_post(post,action,res) {
        try {
            if ( "docid" in post )
                post.docid = utils.normalise_path(post.docid);
            if ( "version1" in post )
                post.version1 = utils.normalise_path(post.version1);
            let obj = action(ecdosis.root,post);
            if ( obj==undefined )
                obj = null;
            ecdosis.print_json_success(res,obj);
        }
        catch ( err ) {
            return ecdosis.print_json_error(res,err,500);
        }
    }
}
ecdosis.root = '/var/local/ecdosis';
ecdosis.web_root = "/var/www";
ecdosis.version = "0.9.0";
ecdosis.port = 8082;
ecdosis.get_api_index = {
    '/project/list':project.list_projects,// tested
    '/ecdosis/menu':menu_cache.get// tested
};
ecdosis.post_api_index = {
    '/splitter/scan':splitter.scan,// tested
    '/splitter/split':splitter.split,// tested
    '/splitter/update':splitter.update,// tested
    '/splitter/layers':splitter.save_layers// tested
};
/**
 * Run the web-application and web-server in the same loop. 
 * 
 * We distinguish web API calls from website fetches by their http request paths. 
 * If a request matches a known path it is processed via the API. The lookup
 * for this is in the post_api_index and the get_api_index. GET parameters are 
 * parsed via the url package. Multipart POST requests are parsed via formidable, 
 * url-encoded ones through querystring. Params are stored in objects passed to 
 * each api call. Files are stored in post.files. All api calls return plain 
 * objects with at least 1-2 fields: success: true or false, and if false also 
 * error: which is a message. The other fields in the object contain data.
 * 
 * If it doesn't match an api path the request falls through to the web-server,
 * which attempts to fetch the resource. If it is a directory it retrieves 
 * <dir>/index.html. If it is not present it returns a 404. If it is present 
 * it works out the mime type via the file suffix and returns the contents.
 *
 * One caveat is that a request from any source ip-address other than localhost 
 * causes the program to terminate. This is to prevent it from being run on a
 * server on the Internet. It has no security because it is designed 
 * to run on the user's local machine only. You already login to your machine, 
 * and not in addition into Ecdosis, which would be silly. So we don't need to 
 * manage users and we don't need certificates or https. Those features can be 
 * added to the web-server and to the published edition if needed. But a 
 * published edition is read-only.
 *
 * Ecdosis performs the following tasks in one application:
 * application-server - formerly tomcat
 * web server - formerly apache, including website configs and proxy
 * database - filebase replaces mongodb document database and mongo db driver
 * spellchecker - MDAG dicts are produced by a separate tool and read here
 * search engine - see search class and sub-classes
 * installer - formerly apt, now replaced by node's pkg tool
 * certbot certificates - not needed because there is no security
 */
class multi_http_server{
    constructor(){
        let hosts_path = path.join(ecdosis.root,"hosts.json");
        // default hosts lookup table
        this.hosts = {"localhost":"splitter"};
        if ( fs.existsSync(hosts_path) ) {
            let j_str = fs.readFileSync(hosts_path);
            this.hosts = JSON.parse(j_str);
        }
        // lookup table of file-suffixes to mime-types
        this.content_types = {'html':'text/html','jpg':'image/jpeg','png':'image/png','js':'text/javascript',
        'json':'application/json','ico':'image/vnd.microsoft.icon','css':'text/css','woff2':'font/woff2'};
        var self = this;
        this.server = http.createServer(function (req, res) {
            // stop maniacs running this as a web server for shared use
            let src_ip_address = req.connection.remoteAddress;
            if ( !utils.ends_with(src_ip_address,"127.0.0.1") && !utils.ends_with(src_ip_address,"::1") )
                throw "for security reasons Ecdosis only runs on localhost";
            // POST is always an API call
            if (req.method === 'POST') {
                if ( "content-type" in req.headers && req.headers['content-type'].indexOf("multipart/form-data")!=-1 ){
                    const form = formidable({ multiples: true });
                    form.parse(req, (err, fields, files) => {
                        if ( err )
                            console.log(err);
                        else {
                            let post = {};
                            for ( let key of Object.keys(fields) )
                                post[key] = fields[key];
                            post.files = files;
                            ecdosis.handle_post(post,ecdosis.post_api_index[req.url],res);
                        }
                    });
                }
                else { //url-encoded
                    var body = '';
                    req.on('data', function (data) {
                        body += data;
                        if (body.length > 1e6)
                            req.connection.destroy();
                    });       
                    req.on('end', function () {
                        var post = qs.parse(body);
                        post.web_root = ecdosis.web_root;
                        ecdosis.handle_post(post,ecdosis.post_api_index[req.url],res);
                    });
                }
            }
            else { 
                // GET is either an API call or a vanilla web server fetch
                let q = url.parse(req.url, true);
                if ( q.pathname in ecdosis.get_api_index ){
                    //console.log("handling "+q.pathname);
                    ecdosis.handle_get(q.query,ecdosis.get_api_index[q.pathname],res);
                }
                else {
                    // decide which website to serve
                    let host_root = "splitter";
                    let host_name = req.headers.host;
                    let bare_host_name = "";
                    // strip out port-number
                    let colon = host_name.indexOf(":");
                    if ( colon != -1 ){
                        bare_host_name = host_name.substring(0,colon);
                    }
                    // convert host-name to directory name and determine file-path
                    if ( bare_host_name in self.hosts )
                        host_root = self.hosts[bare_host_name];
                    let file_path = path.join(ecdosis.web_root,host_root,q.pathname);
                    if ( file_path.indexOf("%")!= -1)
                        file_path = decodeURI(file_path);
                    // determine mime-type from file suffix
                    let dot_pos = q.pathname.lastIndexOf('.');
                    let mime_type = 'text/html';
                    if ( dot_pos != -1 ){
                        let suffix = q.pathname.substring(dot_pos+1);
                        //console.log(suffix);
                        if ( suffix in self.content_types )
                            mime_type = self.content_types[suffix];
                        else // tell us about new file types
                            console.log("warning: unknown content type served as html:"+suffix);
                    }
                    // our only default file name: index.html
                    if ( fs.existsSync(file_path) && fs.statSync(file_path).isDirectory() )
                        file_path = path.join(file_path,"index.html");
                    // the resource may still not be there
                    fs.readFile(file_path, function(err, data) {
                        if (err) {
                            res.writeHead(404, {'Content-Type': 'text/html'});
                            return res.end("404 Not Found:"+err);
                        } 
                        res.writeHead(200, {'Content-Type': mime_type});
                        res.write(data);
                        return res.end();
                    });
                }
            }
        }); 
        this.server.listen(ecdosis.port);
    }
}
// main
// parse commandline arguments
for ( let i=2;i<process.argv.length;i+=2 ) {
    if ( process.argv[i].indexOf('-')==0 && i < process.argv.length-1 ){
        let rest = process.argv[i].substring(1);
        if ( rest == "w" ){
            ecdosis.web_root = process.argv[i+1];
        }
        else if ( rest == 'e' ){
            ecdosis.root = process.argv[i+1];
        }
        else if ( rest == 'p' ){
            if (utils.is_number(argv[i+1]) )
                ecdosis.port = parseInt(process.argv[i+1]);
            else
                throw "port-number not a number:"+argv[i+1];
        }
        else {
            console.log("usage: ecdosis [-e <ecdosis_root>] [-w <web_root>] [-p port]")
            throw "unknown option "+rest;
        }
    }
}
// launch ecdosis
new multi_http_server();
