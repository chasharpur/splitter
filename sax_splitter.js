/*
Copyright (C) 2021 Desmond Schmidt desmond.allan.schmidt@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

const pattern_scanner = require('./pattern_scanner');
const utils = require('./utils');
const layer = require('./layer');
/**
 * SAX splitter is a simplified version-splitter for TEI-XML
 * Instead of DOM we use a linear SAX parse and record the <em>paths</em>
 * leading into each variable bit. We maintain an index where these
 * paths are mapped to layer-names, e.g. app-rdg@wit=X or subst-del
 */
class sax_splitter extends pattern_scanner {
    /**
     * Create an actual splitter based on the scanner
     */
    constructor( config ) {
        super(config);
        // interim storage for read text/tags
        this.buffer = "";
        this.n_layers = 0;
        /** actual patterns found in scan to be used for split */
        this.patterns = {};
        this.split_layers = Array();
        this.last_rdg;
        this.last_rdg_layer;
    }
    /**
     * Scan a TEI-XML file to find layers
     * @param tei the TEI content containing inline versions
     * @return an analysis of the variant markup in a file
     */
    scan_for_layers( tei ) {
        // we don't share the same handlers so create a separate instance 
        let ps = new pattern_scanner(this.config);
        let found = ps.scan(tei);
        for ( let pattern of found ) {
            if ( pattern.hasOwnProperty("layer") ) {
                this.patterns[pattern.path] = pattern;
                let layer_no = pattern.layer;
                if ( layer_no > this.n_layers )
                    this.n_layers = layer_no;
            }
        }
        // create the layers index == layer_no-1
        for ( let i=0;i<this.n_layers;i++ )
            this.split_layers.push(new layer());
    }
    /**
     * Split a TEI-XML file into versions of XML
     * @param tei the TEI content containing versions
     * @return an analysis of the variant markup in a file
     */
    split( tei ) {
        this.line_no = 1;
        this.splits = {};
        this.attributes = {};
        this.last_path = "";
        this.path = "";
        // hard-wire config for now
        this.attributes.rdg = "wit";
        this.attributes.lem = "wit";
        this.splits.add = true;
        this.splits.del = true;
        this.splits.abbrev = true;
        this.splits.expan = true;
        this.splits.rdg = true;
        this.splits.lem = true;
        this.splits.app = true;
        this.splits.mod = true;
        this.splits.choice = true;
        this.splits.subst = true;
        this.parse(tei);
        if ( this.buffer.length> 0 ) {
            for ( let i=0;i<this.n_layers;i++ ) {
                let l = this.split_layers[i];
                l.add( this.buffer );
            }
        }
    }
    /**
     * Get the number of layers needed - called after scan
     * @return a number of layers of simplified XML
     */
    get_layers() {
        return this.split_layers;
    }
    /**
     * Some plain text between tags has been seen
     * @param text the text fragment
     */
    characters( text ) {
        let ws = utils.is_whitespace(text);
        if ( !ws ) {
            if ( this.path.length>0 ) {
                let p_str = this.path;
                if ( p_str == this.last_path )
                    this.last_path = this.last_sibling = "";
            }
        }
        if ( text.indexOf("&")!=-1 )
            text = text.replace(/&/g,"&amp;");
        this.buffer += text;
    }
    /**
     * A start tag has been seen
     * @param name the tag name
     * @param atts the attributes of the tag
     */
    start_element(name, atts) {
        let old_path = this.path;
        if ( !this.splits.hasOwnProperty(name) ) {
            this.buffer += this.build_start_tag(name,atts);
        }
        else if ( this.buffer.length>0 ) {
            if ( this.patterns.hasOwnProperty(old_path) ) {
                let j_obj = this.patterns[old_path];
                let layer_no = j_obj.layer;
                let prev_path = this.ultimate_path();
                let l = this.split_layers[layer_no-1];// NB: 0-indexed
                //System.out.println(buffer.toString()+":"+oldPath);
                l.add(this.buffer);
                // add the text to additional layers
                if ( name == "del" 
                    && layer_no < this.n_layers 
                    && prev_path == "del" ) {
                    let l2 = this.split_layers[layer_no];//i.e. next layer
                    l2.add(this.buffer);
                }
                else if ( this.prev_path == "add" && !this.has_ancestor("del") ) {
                    for ( let i=layer_no;i<nLayers;i++ ) {
                        let l3 = this.split_layers[i];
                        l3.add(this.buffer);
                    }
                }
            }
            else { // common to all layers
                for ( let i=0;i<this.n_layers;i++ ) {
                    let l4 = this.split_layers[i];
                    l4.add(this.buffer);
                }
            }
            this.buffer = "";
        }
        super.start_element(name, atts);
    }
    /**
     * Does the buffer end with a specific start tag
     * @param tag the tag to test for
     * @return true if it ends in that start tag else false
     */
    has_start_tag_at_end( tag ) {
        let index = this.buffer.lastIndexOf("<");
        if ( index != -1 ) {
            let first = this.buffer.charAt(index+1);
            if ( utils.is_letter(first) ) {
                let index2 = this.buffer.lastIndexOf(">");
                if ( index2 == this.buffer.length-1 ) {
                    let tag_name = this.buffer.substring(index+1,index2);
                    return tag_name == tag;
                }
            }
        }
        return false;
    }
    /**
     * Convert the trailing start tag in buffer to an empty tag
     * @param filler generated content for empty tag or an empty string
     * @param tag_name the name of the tag
     */
    convert_empty_tag( filler, tag_name ) {
        let end = this.buffer.length;
        if ( filler.length > 0 ) {
            this.buffer += filler;
            this.buffer += "</";
            this.buffer += tag_name;
            this.buffer += ">";
        }
        else
            this.buffer = this.buffer.substring(0,end-1)+"/>";
    }
    end_element(name) {
        if ( !this.splits.hasOwnProperty(name) ) {
            if ( this.has_start_tag_at_end(name) ) {
                let filler = "";
                if ( name == "unclear" )
                    filler = "[unclear]";
                this.convert_empty_tag(filler,name);
            }
            else 
                this.buffer += this.build_end_tag(name);
        }
        else  {
            if ( name == "app" ) {
                if ( !this.has_ancestor("del") ) {
                    for ( let i=this.last_rdg_layer;i<this.n_layers;i++ ) {
                        let l = this.split_layers[i];
                        l.add(this.last_rdg);
                    }
                }
                this.last_rdg_layer = this.n_layers;
                this.last_rdg = "";
            }
            if ( this.buffer.length>0 ) {
                if ( this.patterns.hasOwnProperty(this.path) ) {
                    let j_obj = this.patterns[this.path];
                    let layer_no = j_obj.layer;
                    let l = this.split_layers[layer_no-1];
                    l.add(this.buffer);
                    if ( name == "add" && !this.has_ancestor("del") ) {
                        for ( let i=layer_no;i<this.n_layers;i++ ) {
                            l = this.split_layers[i];
                            l.add(this.buffer);
                        }
                    }
                    else if ( name == "rdg" ) {
                        this.last_rdg = this.buffer;
                        this.last_rdg_layer = layer_no;
                    }
                    else if ( name == "del" 
                        && this.last_popped_path == "del" ) {
                        let l2 = this.split_layers[layer_no];//i.e. next layer
                        l2.add(this.buffer);
                    }
                }
                else // can't throw exception here
                    console.log("No pattern found for "+this.path+": "+this.buffer);
                this.buffer = "";
            }
        }
        super.end_element(name);
    }
}
module.exports = sax_splitter;
